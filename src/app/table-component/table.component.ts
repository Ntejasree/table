import { Component, OnInit } from '@angular/core';
import users from '../../assets/sample_data.json';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent implements OnInit {
  public title = 'TestTable';
  public users: any[] = users;
  public columns: string[]    = ['Name', 'Phone', 'E-mail', 'Company', 'Street', 'City', 'Zip Code', 'Fee', 'status'];
  public columnKeys: string[] = ['name', 'phone', 'email', 'company', 'address_1', 'city', 'zip', 'fee', 'status'];
  public activePage = 0;
  public numberOdRecords = 10;
  public tempUsers: any[];

  constructor() {}

  ngOnInit(): void {
    this.tempUsers = this.users.slice(0, this.numberOdRecords);
  }

  /**
   * Based on page number triggred by pagination component need to udpate the values in table
   * @param activePageNumber page number give by pagination component
   */
  displayActivePage(activePageNumber: number) {
    this.activePage = activePageNumber;
    const start = activePageNumber * this.numberOdRecords - this.numberOdRecords;
    const end = activePageNumber * this.numberOdRecords;
    this.tempUsers = this.users.slice(start, end);
  }
}
