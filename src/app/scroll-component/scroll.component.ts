import { Component, OnInit, HostListener } from '@angular/core';
import users from '../../assets/sample_data.json';

@Component({
  selector: 'app-tablescroll',
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.sass']
})
export class ScrollComponent implements OnInit {

  public title = 'TestTable';
  public users: any[] = users;
  public columns: string[]    = ['Name', 'Phone', 'E-mail', 'Company', 'Street', 'City', 'Zip Code', 'Fee', 'status'];
  public columnKeys: string[] = ['name', 'phone', 'email', 'company', 'address_1', 'city', 'zip', 'fee', 'status'];
  public activePage = 0;
  public numberOdRecords = 15;
  public tempUsers: any[];

  constructor() {}

  ngOnInit(): void {
    this.tempUsers = this.users.slice(0, this.numberOdRecords);
    this.activePage = 1;
  }

  /**
   * Used for to determine the scrollerbar position and update users list with new data
   * @param event Scroller event to determine the position of the scroller in view
   */
  @HostListener('scroll', ['$event'])
  displayActivePage(event: any) {
    if (Math.ceil(event.target.offsetHeight + event.target.scrollTop) >= event.target.scrollHeight) {
      const start = this.activePage * this.numberOdRecords - this.numberOdRecords;
      const end = this.activePage * this.numberOdRecords;
      if (this.tempUsers.length > 0){
        if (this.users[start] !== undefined) {
          this.tempUsers = this.tempUsers.concat(this.users.slice(start, end));
          this.activePage++;
        }
      } else {
        this.tempUsers = this.users.slice(start, end);
      }
    }
  }
}
