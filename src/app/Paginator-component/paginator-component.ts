import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator-component.html',
  styleUrls: ['./paginator-component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class PaginatorComponent implements OnChanges {
  @Input() totalRecords = 0;
  @Input() recordsPerPage = 0;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();

    public pages: number [] = [];
    activePage: number;

    ngOnChanges(){
      const pageCount = this.getPageCount();
      this.pages = this.getArrayOfPage(pageCount);
      this.activePage = 1;
      this.pageChange.emit(1);
    }
    /**
     * To caliculate page count used to device the pagination
     */
    private  getPageCount(): number {
      let totalPage = 0;
      if (this.totalRecords > 0 && this.recordsPerPage > 0){
        const pageCount = this.totalRecords / this.recordsPerPage;
        const roundedPageCount = Math.floor(pageCount);
        totalPage = roundedPageCount < pageCount ? roundedPageCount + 1 : roundedPageCount;
      }
      return totalPage;
    }

    /**
     * Funciton used to devide and create the links for pagination based on pagecount
     * @param pageCount how many pages can be devided
     */
    private getArrayOfPage(pageCount: number): number [] {
      const pageArray: number [] = [];
      if (pageCount > 0){
        for (let i = 1; i <= pageCount; i++) {
          pageArray.push(i);
        }
      }
      return pageArray;
    }
    /**
     * When any link in pagination is cliked respective event with page number will be triggred
     * @param pageNumber selected page number
     */
    onClickPage(pageNumber: number){
        if (pageNumber < 1) {
          return;
        }
        if (pageNumber > this.pages.length) {
          return;
        }
        this.activePage = pageNumber;
        this.pageChange.emit(this.activePage);
    }
}
