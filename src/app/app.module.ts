import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PaginatorComponent } from './Paginator-component/paginator-component';
import { ScrollComponent } from './scroll-component/scroll.component';
import { TableComponent } from './table-component/table.component';

@NgModule({
  declarations: [
    AppComponent,
    PaginatorComponent,
    TableComponent,
    ScrollComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
