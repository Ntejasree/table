import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table-component/table.component';
import { ScrollComponent } from './scroll-component/scroll.component';


const routes: Routes = [
  { path: 'table', component: TableComponent },
  { path: 'scroll', component: ScrollComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
